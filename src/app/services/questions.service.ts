import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class QuestionsService {

  constructor(private http: HttpClient) { }

  /** GET questions json from the testapi */
  getQuestions(): Observable<any> {
    return this.http.get<any>(`${environment.baseUrl}/questions`);
  }

}
