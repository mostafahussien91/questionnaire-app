import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { QuestionsService } from './questions.service';
import { environment } from 'src/environments/environment';

describe('QuestionsService', () => {
  let service: QuestionsService;
  let httpMock: HttpTestingController;

  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [QuestionsService]
  }));

  beforeEach(() => {
    service = TestBed.get(QuestionsService);
    httpMock = TestBed.get(HttpTestingController);

  });

  it('should retrieve questions from the testapi and the result is array with length greater than 0', () => {
    service.getQuestions().subscribe(res => {
      expect(res.length).toBeGreaterThan(0);
    });
    const request = httpMock.expectOne(`${environment.baseUrl}/questions`);
    expect(request.request.method).toBe('GET');
  });
});
