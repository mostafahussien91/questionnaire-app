export interface Question {
    question_type: string;
    multiple: boolean;
    required: boolean;
    identifier: string;
    headline: string;
    description: string;
    choices: [];
}
