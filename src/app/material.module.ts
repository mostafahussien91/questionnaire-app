import { NgModule } from '@angular/core';

// Angular material components
import {
    MatRadioModule,
    MatInputModule,
    MatButtonModule,
    MatProgressBarModule
} from '@angular/material';

@NgModule({
    exports: [
        MatRadioModule,
        MatInputModule,
        MatButtonModule,
        MatProgressBarModule
    ]
})

export class MaterialModule { }
