import { Component, OnInit } from '@angular/core';
import { QuestionsService } from '../../services/questions.service';
import { Question } from '../../models/question.model';
import { filter } from 'rxjs/operators';
@Component({
  selector: 'app-questions-list',
  templateUrl: './questions-list.component.html',
  styleUrls: ['./questions-list.component.scss']
})
export class QuestionsListComponent implements OnInit {

  questionsList: Question[] = [];

  qIndex = 0;

  selection = [];

  qTitle;

  qDesc;

  animateQ = true;

  constructor(private questions: QuestionsService) { }

  ngOnInit() {
    this.getQuestions();
    setTimeout(() => {
      this.animateQ = false;
    }, 1000);
  }

  getQuestions() {
    this.questions.getQuestions().subscribe(data => {
      this.questionsList = data.questionnaire.questions;
      this.qTitle = data.questionnaire.name;
      this.qDesc = data.questionnaire.description;
    });
  }

  nextQuestion() {
    this.animateQ = false;
    setTimeout(() => {
      this.animateQ = true;
    }, 50);
    if (this.qIndex >= 0 && this.qIndex <= this.questionsList.length) {
      this.qIndex++;
    } else {
      this.qIndex = 0;
    }
  }

  prevQuestion() {
    this.animateQ = false;
    setTimeout(() => {
      this.animateQ = true;
    }, 50);
    if (this.qIndex !== 0 && this.qIndex <= this.questionsList.length) {
      this.qIndex--;
    } else {
      this.qIndex = 0;
    }
  }

  onChange(event, index) {
    if (this.questionsList[index].question_type === 'text') {
      this.selection[index] = event.target.value;
    } else {
      this.selection[index] = event.value;

    }
  }

}
