import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionsListComponent } from './questions-list.component';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '../../material.module';
import { HttpClientModule } from '@angular/common/http';
import { By } from '@angular/platform-browser';


describe('QuestionsListComponent', () => {
  let component: QuestionsListComponent;
  let fixture: ComponentFixture<QuestionsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, FormsModule, MaterialModule, HttpClientModule],
      declarations: [QuestionsListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should start with a questions array index at `0`', () => {
    expect(component.qIndex).toEqual(0);
  });

  it('should start with a questions array length greater than `0`', () => {
    expect(component.questionsList.length).toEqual(0);
  });

  it('should increment the qindex if increment function is clicked (+1)', () => {

    fixture.detectChanges();

    component.nextQuestion();

    expect(component.qIndex).toEqual(1);
  });

  it('should increment the qindex if decrement function is clicked (-1)', () => {

    fixture.detectChanges();

    component.prevQuestion();

    expect(component.qIndex).toEqual(0);
  });

});
